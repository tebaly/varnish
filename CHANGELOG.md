# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.1](https://gitlab.com/tebaly/varnish/compare/v0.1.0...v0.1.1) (2021-10-25)


### Bug Fixes

* branches ([52e9d54](https://gitlab.com/tebaly/varnish/commit/52e9d54f81c35ec5446276c58c2c56b63a4cfc29))
* ver ([64ad5ce](https://gitlab.com/tebaly/varnish/commit/64ad5ceeb4827f5a97ba39329957539b672ac931))

## 0.1.0 (2021-10-25)


### Bug Fixes

* lib var (mods) ([d9e998d](https://gitlab.com/tebaly/varnish/commit/d9e998d92b57a3f709cbac2a82884de240575ba0))
